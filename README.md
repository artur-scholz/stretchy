# StretchyDb

StretchyDb is an open source REST API database designed to be simple in use. The
basic CRUD (create, read, update, delete) operations are available for getting
data in and out from the database. Data is stored as JSON documents (composed
of key-value fields) into models and grouped by domain.

StretchyDb is designed to be able to use different backend database technologies.
So for howver, only MongoDb is implemented as backend.

## Getting Started

Install StretchyDb:

```bash
$ pip install git+https://gitlab.com/librecube/lib/python-stretchydb.git
```

To start StretchyDb:

```python
from stretchydb import StretchyDb
app = StretchyDb()
app.run()
```

> Make sure to have a backend database installed and running. The default backend
database is [mongoDB](https://www.mongodb.com/).

Then navigate with your browser to http://localhost:5000 to see the content of
your database. As StretchDb is a Flask app, you can configure it accordingly;
for example to run on port 7979 use `app.run(port=7979)`.

You may use curl or any other REST client to move data and and out of the database:

```
GET /myhouse/temperature
```

The result is:

```json
  [
    {
      "_id": "5b87bfef2ce70e0008668ba4",
      "time": "2018-08-30T11:09:28.411410",
      "value": 25.5,
    },
    {
      "_id": "25b87bfef2ce70e0008668ba5",
      "time": "2018-08-30T11:09:49.493020",
      "value": 25.7,
    },
  ]
```

To add a new entry into your ToDo list:

```json
  POST /my-docs/todo
  [
    {
      "title": "StretchyDB documentation",
      "description": "There is always something missing. Add more details!",
      "deadline": "soon"
    }
  ]
```

Have a look at the [Tutorial](docs/tutorial.md) for more examples.
