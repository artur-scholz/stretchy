import os
from enum import Enum

from flask import Flask, send_from_directory
from .http import register_error_handlers


class BackendDatabase(Enum):
    MONGO_DB = 'mongo_db'


class StretchyDb(Flask):

    def __init__(
            self,
            backend_db=BackendDatabase.MONGO_DB,
            backend_host='localhost',
            backend_port=27017):

        super().__init__(__package__)

        @self.route('/favicon.ico')
        def favicon():
            return send_from_directory(
                os.path.join(self.root_path, 'static'), 'favicon.ico',)

        register_error_handlers(self)

        if backend_db == BackendDatabase.MONGO_DB:
            os.environ["MONGO_DB_HOST"] = backend_host
            os.environ["MONGO_DB_PORT"] = str(backend_port)
            from .backend.mongo_db import MongoDb
            MongoDb(self)

        else:
            raise NotImplementedError
