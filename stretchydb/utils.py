import bson
from flask import jsonify, abort


def to_json(documents):
    """
    Returns a valid JSON object from a list of dictionaries
    """
    if isinstance(documents, dict):
        output = {}
        for key, value in documents.items():
            if isinstance(value, bson.ObjectId):
                value = str(value)
            output[key] = value
    else:
        output = []
        for document in documents:
            field = {}
            for key, value in document.items():
                if isinstance(value, bson.ObjectId):
                    value = str(value)
                field[key] = value
            output.append(field)
    return jsonify(output)


def to_objectid_or_int(_id):
    try:
        _id = bson.ObjectId(_id)
    except bson.errors.InvalidId:
        try:
            _id = int(_id)
        except ValueError:
            abort(400, 'The _id is not of a valid format. '
                       'Allowed are ObjectId or int.')
    return _id
