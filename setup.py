from setuptools import setup

setup(
    name='StretchyDb',
    version='2.0.0',
    description='A document-based database with REST API',
    license='GPL',
    python_requires='>=3.4',
    keywords='REST API NoSQL database',
    packages=['stretchydb'],
    install_requires=[
        'flask',
        'flask-restful',
        'pymongo'
    ],
)
