# Tutorial

The REST API provides basic CRUD (create, read, update, and delete) operations
for managing entries in the database.

## POST

In order to add data (in the form of JSON documents) to the database,
issue a POST request with a single document or a list of documents in the
request body to a domain/model endpoint.

Let's create a new document in the model *best-movies* in the domain *cinema*.

```json
POST /cinema/best-movies
{
    "title": "The Wizard of Oz",
    "year": 1939,
    "rank": 1,
    "reviews": 111
}
```

> Values are stored in the datatype that they are provided. An integer
is saved as integer, float as float, string as string etc. One exception is
for datetime objects. Those must be converted to strings. The recommended format
is ISO datetime format, such as "2018-08-30T11:09:28.411410"

Successful write operations are confirmed with an HTTP 201 status and the
following JSON body:

```json
{
    "info": "created"
}
```

> Writing to domains and/or models that do not exist will create them.

> An `_id` field is generated automatically when new documents are added
  to a model. It is a string of hex values that is unique within that model.
  You must **not** supply a value for `_id` in the document.

Create two new documents in a domain model.

```json
POST /cinema/best-movies
[
    {
        "title": "Citizen Kane",
        "year": 1941,
        "rank": 2,
        "reviews": 75
    },
    {
        "title": "The Third Man",
        "year": 1949,
        "rank": 3,
        "reviews": 77
    }
]
```

Add yet another document, this time supplying adding another field and
leaving one out.

```json
POST /cinema/best-movies
{
    "title": "Get Out",
    "year": 2017,
    "rank": 4,
    "note": "Comedy and horror"
}
```
> As shown, documents within a domain can have different fields.

## PUT

To replace an existing document, use a PUT request with the new document in the
request body. You can only replace a document that exists and you must know its
`_id` value to form the URL endpoint.

```json
PUT /cinema/best-movies/25b87bfef2ce90e0108668ba5
{
    "title": "Just a test",
    "hello": "world",
}
```

This replaces the document with the `_id` = 25b87bfef2ce90e0108668ba5 and gives
an status code of 200, providing that the document existed already. If not,
a not found error (404) will be returned.

> You must **not** supply a value for `_id` in the document.

## PATCH

To modify an existing document (that is, to add/change selected fields), use a
PATCH request with a document that encompasses the fields to be modified in the
request body. You can only modify a document that exists and you must know its
`_id` value to form the URL endpoint.

```json
PATCH /cinema/best-movies/25b87bfef2ce90e0108661ba9
{
    "title": "Get Out",
    "year": 2007,
    "rank": 4,
    "note": "Comedy and horror"
}
```

This modifies the document with the `_id` = 25b87bfef2ce90e0108661ba9 and gives
an status code of 200, providing that the document existed already. If not,
a not found error (404) will be returned.

> You must **not** supply a value for `_id` in the document.

The result of this operation is that the provided fields will be added to or
overwrite fields in the existing document, while keeping all other fields.

## GET

Successful read operations are confirmed with a HTTP 200 status code and have
a JSON object in the response body that contains one or more documents.

Get a list of all defined domains:

```
GET /
```

```
[
    'cinema',
    'my-docs',
    'marketing'
]
```

Get a list of all models in a domain:

```
GET /cinema
```

```
[
    'best-movies',
    'new-movies',
    'top-actors',
]
```

Get a specific document:

```
GET /cinema/best-movies/1
```

```
{
    "_id": "25b87bfef2ce90e0808661ba9",
    "title": "The Wizard of Oz",
    "year": 1939,
    "rank": 1,
    "reviews": 111
}
```

> Documents are identified through their unique `_id` field.

Get all documents of a model of a domain:

```
GET /cinema/best-movies
```

```
  [
    {
      "_id": "5b7eb2b348e7552fdc54a317",
      "title": "The Wizard of Oz",
      "year": 1939,
      "rank": 1,
      "reviews": 111
    },
    {
      "_id": "5b7eb2b348e7552fdc54a318",
      "title": "Citizen Kane",
      "year": 1941,
      "rank": 2,
      "reviews": 75
    },
    {
      "_id": "5b7eb2b348e7552fdc54a319",
      "title": "The Third Man",
      "year": 1949,
      "rank": 3,
      "reviews": 77
    },
    {
      "_id": "5b7eb2b348e7552fdc54a31a",
      "title": "Get Out",
      "year": 2007,
      "rank": 4,
      "note": "Comedy and horror"
    }
  ]
```

### Filtering

Search for documents with distinct years:

```
GET /cinema/best-movies?_id=2001,2005,2009
```

Filters can be concatenated. Search for movies from a specific year with same
reviews:

```
GET /cinema/best-movies?year=1939&reviews=111
```

Next to these exact matches, one can also use the following operators:

==============    ===============
Operator          Description
==============    ===============
in                same as exact match
lt                lower than
le                lower than or equal
gt                greater than
ge                greater than or equal
==============    ===============

To get the movies in the top ten ranking after the year 2000, issue the following:

```
GET /cinema/best-movies?year=ge:2000&rank=le:10
```

### Paging and Limiting

To obtain only a subset of the queryed results, pagination can be used. The number
of documents to return is controlled via the `_pagesize` parameter, whose default
is 100. The page to return is specified with the `_page` parameter.

> Page numbering starts from page 1.

For instance, to return documents from 20 to 29 (page 3):

```
GET /cinema/best-movies?_page=3&_pagesize=10
```

To simply limit the result to n documents, use the `_limit` parameter:

```
GET /cinema/best-movies?_limit=10
```

### Projection

Projection limits the fields to return for all matching documents, specifying
which fields to be returned. This is done via the `_fields` parameter.

Only get the title and year of movies:

```
GET /cinema/best-movies?_fields=title,year
```

> The `_id` field is always included in the returned documents.

### Sorting

Sorting of the returned documents is done via the `_sort` parameter. To sort
for several fields, pass them as a list. To sort in descending order (from large
to small), prefix the field name with a minus sign. Default is ascending order,
for which a plus sign can be used optionally.

For example, to sort for years in descending order and then for rank in ascending
order:

```
GET /cinema/best-movies?_sort=-year,rank
```

## DELETE Operations

The delete operations can be applied to individual documents, a range of documents,
the full model, and even the entire domain.

To delete a single document, e.g. document with `_id` = 5b7eb2b348e7552fdc54a31b:

```
DELETE /cinema/best-movies/5b7eb2b348e7552fdc54a31b
```

Successful delete operations are confirmed with an HTTP 200 status and the
following JSON body:

```json
{
    "info": "deleted"
}
```

To delete all documents that have a year before 2000 (see READ section for details
on filtering), issue:

```
DELETE /cinema/best-movies?year=lt:2000
```

To delete a model and domain, issue the following statements, respectively:

```
DELETE /cinema/best-movies
DELETE /cinema
```

## HTTP Errors

A few HTTP error codes and messages are defined by the REST API.

- Providing a malformed request (for example, a POST request without JSON body)
  will return an HTTP 400 status.
- Providing a request with unexpected/forbidden values (for example, providing an
  `_id` field in POST/PUT/PTACH requests) will return an HTTP 422 status.
- Trying to read a non-existing resource returns an HTTP 404 status.
